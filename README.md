This is a simple calculator made of Django with rest_frameworks_apis

Specifications
Uses the 4 HTTP methods for each operation. 
User will input the 2 numbers . Use GET, PUT, POST, DELETE for +,-,*,/ respectively.
The api is token authenticated. 
The api will return a “message” whether Successful or Unsuccessful. It will also return the result(obviously) and a status code of 200 for success, 400 for failure or invalid operation, and 401/403 on auth failure.
