from rest_framework import serializers
from . models import calculator
from rest_framework import exceptions

class SerializeCalculator(serializers.ModelSerializer):
    class Meta:
        model=calculator
        fields=['digit1','digit2','res','operation']

