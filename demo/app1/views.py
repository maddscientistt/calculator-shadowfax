from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from . models import calculator
from .serializers import SerializeCalculator
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated


class calc(APIView):
    @permission_classes((IsAuthenticated,))
    def get(self,request):
        # calc= calculator.objects.all()
        # serializer=calculatorSerializer(calc,many=True)
        # return Response(serializer.data)
        val1= request.data.get('n1',"")
        val2= request.data.get('n2',"")
        if val1=="":
            return Response({'Unsuccesful, please enter 1st digit'},status=status.HTTP_400_BAD_REQUEST)
        if val2=="":
            return Response({'Unsuccesful, please enter 2nd digit'},status=status.HTTP_400_BAD_REQUEST)

        try:
            obj = calculator.objects.get(digit1=val1,digit2=val2,operation="add")
        except calculator.DoesNotExist:    
            res=int(val1)+int(val2)
            query = calculator(digit1=val1,digit2=val2,res=res,operation="add")
            query.save()
            return Response(res)
        serialize = SerializeCalculator(obj)
        return Response(serialize.data['res'])


        # return Response({"message" : "successfull", "status":200, "result": res})


    def put(self,request):
        val1= request.data.get('n1',"")
        val2= request.data.get('n2',"")
        if val1=="":
            return Response({'Unsuccesful, please enter 1st digit'},status=status.HTTP_400_BAD_REQUEST)
        if val2=="":
            return Response({'Unsuccesful, please enter 2nd digit'},status=status.HTTP_400_BAD_REQUEST)

        try:
            obj = calculator.objects.get(digit1=val1,digit2=val2,operation="sub")
        except calculator.DoesNotExist:    
            res=int(val1)-int(val2)
            query = calculator(digit1=val1,digit2=val2,res=res,operation="sub")
            query.save()
            return Response(res)
        serialize = SerializeCalculator(obj)
        return Response(serialize.data['res'])


    def post(self,request):
        val1= request.data.get('n1',"")
        val2= request.data.get('n2',"")
        if val1=="":
            return Response({'Unsuccesful, please enter 1st digit'},status=status.HTTP_400_BAD_REQUEST)
        if val2=="":
            return Response({'Unsuccesful, please enter 2nd digit'},status=status.HTTP_400_BAD_REQUEST)

        try:
            obj = calculator.objects.get(digit1=val1,digit2=val2,operation="mul")
        except calculator.DoesNotExist:    
            res=int(val1)*int(val2)
            query = calculator(digit1=val1,digit2=val2,res=res,operation="mul")
            query.save()
            return Response(res)
        serialize = SerializeCalculator(obj)
        return Response(serialize.data['res'])


    def delete(self,request):
        val1= request.data.get('n1',"")
        val2= request.data.get('n2',"")
        if val1=="":
            return Response({'Unsuccesful, please enter 1st digit'},status=status.HTTP_400_BAD_REQUEST)
        if val2=="":
            return Response({'Unsuccesful, please enter 2nd digit'},status=status.HTTP_400_BAD_REQUEST)

        try:
            obj = calculator.objects.get(digit1=val1,digit2=val2,operation="divide")
        except calculator.DoesNotExist:    
            res=int(val1)/int(val2)
            query = calculator(digit1=val1,digit2=val2,res=res,operation="divide")
            query.save()
            return Response(res)
        serialize = SerializeCalculator(obj)
        return Response(serialize.data['res'])


# def home(request):
#     return render(request,"hi.html")

# def add(request):
#     val1= int(request.GET['n1'])
#     val2= int(request.GET['n2'])
#     res=val1+val2

#     return HttpResponse(res)